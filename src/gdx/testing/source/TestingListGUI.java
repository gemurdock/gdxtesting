package gdx.testing.source;

import gdx.testing.source.tests.AspectRatio;
import gdx.testing.source.tests.Spline;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class TestingListGUI extends JFrame {

    private static final long serialVersionUID = 1713506347140065297L;
    private static final String title = "GDX Testing";

    JScrollPane scrollpane;
    JList<String> list;
    
    public static final List<Class<? extends GDXTest>> tests = new ArrayList<Class<? extends GDXTest>>(Arrays.asList(
	    Spline.class,
	    AspectRatio.class
	    ));

    public TestingListGUI() {
	populateList();
	
	scrollpane = new JScrollPane(list);
	
	this.add(scrollpane, BorderLayout.CENTER);
	
	this.setTitle(title);
	this.setSize(400, 500);
	this.setLocationRelativeTo(null);
	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	this.setVisible(true);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void populateList() {
	list = new JList(getNames().toArray());
	list.addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseClicked(MouseEvent event) {
		if(event.getClickCount() == 2) {
		    String testName = (String) list.getSelectedValue();
		    setupTest(testName);
		}
	    }
	});
    }
    
    @SuppressWarnings("rawtypes")
    public void setupTest(String testName) {
	LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
	config.title = title;
	config.useGL30 = false;
	config.width = 800;
	config.height = 480;
	
	GDXTest game = null;
	for(Class test : tests) {
	    if(test.getSimpleName().equals(testName)) {
		try {
		    game = (GDXTest) test.newInstance();
		} catch (InstantiationException e) {
		    e.printStackTrace();
		} catch (IllegalAccessException e) {
		    e.printStackTrace();
		}
	    }
	}
	
	this.setVisible(false);
	this.dispose();

	new LwjglApplication(game, config);
    }
    
    @SuppressWarnings("rawtypes")
    public List<String> getNames() {
	List<String> names = new ArrayList<String>(tests.size());
	for(Class c : tests) {
	    names.add(c.getSimpleName());
	}
	Collections.sort(names);
	return names;
    }

}
