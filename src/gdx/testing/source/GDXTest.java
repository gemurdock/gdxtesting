package gdx.testing.source;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public abstract class GDXTest extends Game {
    
    public GDXTest() {
    }
    
    @Override
    public void render() {
	render(Gdx.graphics.getDeltaTime());
    }
    
    public abstract void render(float delta);

}
