package gdx.testing.source.tests;

import gdx.testing.source.GDXTest;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.Vector2;

public class Spline extends GDXTest {

    ShapeRenderer shapeRenderer;

    CatmullRomSpline<Vector2> catmullrom;
    int k = 150;

    List<Vector2> vectorList = new ArrayList<Vector2>();

    boolean createNewCatmull = false;
    boolean setupCatmull = false;

    boolean continuous = true;
    
    InputAdapter input = new InputAdapter() {
	
	@Override
	    public boolean keyUp (int keycode) {
		if(keycode == Input.Keys.SPACE) {
		    createNewCatmull = true;
		    vectorList.clear();
		    return true;
		}
		if(keycode == Input.Keys.ENTER) {
		    createNewCatmull = false;
		    setupCatmull = true;
		    return true;
		}
		if(keycode == Input.Keys.C) {
		    continuous = continuous ? false : true;
		    System.out.println(continuous);
		    return true;
		}
		return false;
	    }
	    
	    @Override
	    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
		if(createNewCatmull && !setupCatmull) {
		    Vector2 v = new Vector2(screenX, Gdx.graphics.getHeight() - screenY); // Fix coordinate issue
		    vectorList.add(v);
		    return true;
		}
		return false;
	    }
	
    };

    public Spline() {
    }
    
    @Override
    public void create() {
	shapeRenderer = new ShapeRenderer();
	Gdx.input.setInputProcessor(input);
    }

    @Override
    public void render(float delta) {
	update(delta);

	Gdx.gl.glClearColor(0, 0, 0, 1);
	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

	shapeRenderer.begin(ShapeType.Line);
	shapeRenderer.setColor(Color.RED);

	Vector2 out = new Vector2();
	Vector2 out2 = new Vector2();

	if(catmullrom != null) {
	    for(int i = 0; i < k - 1; i++) {
		catmullrom.valueAt(out, (float) i++ / (float) k);
		catmullrom.valueAt(out2, (float) i / (float) k);
		shapeRenderer.line(out, out2);
	    }
	}

	shapeRenderer.end();

	shapeRenderer.begin(ShapeType.Filled);
	shapeRenderer.setColor(Color.BLUE);

	for(int i = 0; i < vectorList.size(); i++) {
	    Vector2 v = vectorList.get(i);
	    shapeRenderer.circle(v.x, v.y, 10);
	}

	shapeRenderer.end();
    }
    
    public void update(float delta) {
	if(setupCatmull) {
	    Vector2[] points = new Vector2[vectorList.size()];
	    for(int i = 0; i < vectorList.size(); i++) {
		points[i] = vectorList.get(i);
	    }
	    catmullrom = new CatmullRomSpline<Vector2>(points, continuous);
	    setupCatmull = false;
	}
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
	shapeRenderer.dispose();
    }

}
