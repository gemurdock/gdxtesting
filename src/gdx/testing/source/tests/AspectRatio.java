package gdx.testing.source.tests;

import gdx.testing.source.GDXTest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class AspectRatio extends GDXTest {
    
    Viewport viewport;
    
    ShapeRenderer renderer;

    @Override
    public void create() {
	viewport = new ExtendViewport(960, 720, 1280, 720, new OrthographicCamera());
	viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	
	renderer = new ShapeRenderer();
    }

    @Override
    public void render(float delta) {
	Gdx.gl.glClearColor(0, 0, 0, 1);
	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	
	// Fill world viewport with color blue
	renderer.begin(ShapeType.Filled);
	renderer.setColor(Color.BLUE);
	renderer.rect(-viewport.getWorldWidth() / 2, -viewport.getWorldHeight() / 2, viewport.getWorldWidth(), viewport.getWorldHeight());
	
	renderer.end();
    }
    
    @Override
    public void resize(int width, int height) {
	viewport.update(width, height, false);
	renderer.setProjectionMatrix(viewport.getCamera().combined);
    }
    
    @Override
    public void dispose() {
	renderer.dispose();
    }

}
